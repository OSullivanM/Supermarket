Decided on creating 2 interfaces as the point of entry to
separate  between the cash til clerk and the duties of the Supermarket
 administrator / manager. I would extend both classes to use some sort
 of authentication.

 Used predominantly a singleton design pattern throughout, separating
 class up between key functionality and domain objects.
 I decided on creating a second module for integration tests.

 For testing I used testng and mockito.

 I tried to keep the use of external libraries to a minimum - small use
 of guava but nothing else other than that.

 In terms of the discounts and offer functionality I tried to keep
 things as generic as possible, allowing the creation of new offers
 through the admin interface. The OfferType enum will give you an
 indication of the offers one can create.

 I used a couple of functional interfaces to compute the gross and net
 totals.

 I used a TDD approach for the most part, in order to add simplicity.

 Limitations etc: I've done nothing with rounding, something I would do.
 Also, in terms of dependency injection, to keep in line with the test
 criteria, nothing was used but would almost certainly use SPRING here.
 Not all the api operations are ar complete. Couldn't afford to spend
 any more time!

I would expose all this through a RESTful api, with the following

URI http://mos.supermarket.com/v1/
URI http://mos.supermarket.com/v1/admin/

resources: POST /createOffer
           GET /addStock
           PUT /deleteOfferOnItem
           PUT /checkout
           GET /scan
           POST /scanAndAddToOrder
           PUT /removeFromOrder
           PUT /clearOrder
           PUT /removeFromOrder




