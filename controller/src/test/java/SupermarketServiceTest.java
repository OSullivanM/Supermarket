import com.mos.supermarket.api.SupermarketServiceImpl;
import com.mos.supermarket.domain.Item;
import com.mos.supermarket.domain.Order;
import com.mos.supermarket.domain.Stock;
import com.mos.supermarket.enums.SupermarketServiceExceptionEnum;
import com.mos.supermarket.exception.SupermarketServiceException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Optional;

import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.testng.Assert.*;

/**
 * Created by mosul on 07/05/2017.
 */
public class SupermarketServiceTest {

    private static final Long ORDER_ID = 1L;
    private static final Long ITEM_ID = 2L;
    private SupermarketServiceImpl supermarketService;
    @Mock private Stock stock;
    @Mock private Order order;

    @BeforeMethod
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        supermarketService = new SupermarketServiceImpl();
    }

    @AfterMethod
    public void tearDown() throws Exception {
        verifyNoMoreInteractions(stock);
    }

    @Test
    public void testScan_ItemFound() throws SupermarketServiceException {
        assertNull(supermarketService.scan(ITEM_ID));
    }

    @Test
    public void testScanAndAddToOrder() throws SupermarketServiceException {
        when(stock.getStockItem(1L)).thenReturn(Optional.empty());
        try {
            assertNull(supermarketService.scanAndAddToOrder(ORDER_ID, ITEM_ID, 1D));
        } catch (SupermarketServiceException e) {
            Assert.assertEquals(e.getErrorCode(), SupermarketServiceExceptionEnum.ITEM_NOT_IN_STOCK);
        }
    }

    @Test
    public void testRemoveFromOrder_OrderNotFound() throws SupermarketServiceException {
        when(order.getOrders()).thenReturn(Collections.emptyMap());
        Item item = new Item(22L, "Unknown Item");
        try {
            supermarketService.removeFromOrder(ITEM_ID, item.getItemId());
            fail();
        } catch (SupermarketServiceException e) {
            assertEquals(e.getErrorCode(), SupermarketServiceExceptionEnum.ORDER_NOT_FOUND);
        }
    }

    @Test
    public void testItemInStock() throws SupermarketServiceException {
        Assert.assertFalse(supermarketService.itemInStock(ITEM_ID));
    }

}