package com.mos.supermarket;

import com.mos.supermarket.domain.StockItem;
import com.mos.supermarket.domain.UnitPrice;

public class TestHelper {

    public static StockItem createStockItem(long itemId, String itemName, UnitPrice price, boolean
        offerAvailable) {
        return new StockItem(itemId, itemName, price, offerAvailable);
    }
}
