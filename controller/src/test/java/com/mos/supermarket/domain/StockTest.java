package com.mos.supermarket.domain;

import static com.mos.supermarket.TestHelper.createStockItem;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

import com.mos.supermarket.exception.SupermarketServiceException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Created by mosul on 08/05/2017.
 */
public class StockTest {

    private Stock stock;

    @BeforeMethod
    public void setUp() throws Exception {
        stock = Stock.getInstance();
    }

    @AfterMethod
    public void tearDown() throws Exception {
        stock.clearAllStock();
    }

    @Test
    public void testAddStock() throws Exception {
        StockItem stockItem = createStockItem(11L, "Soup", UnitPrice.price(1.99d), false);
        stock.addStock(stockItem, 50);
        assertNotNull(stock);
    }

    @Test
    public void testGetStockItem() throws Exception {
        StockItem stockItem = createStockItem(11L, "Soup", UnitPrice.price(1.99d), false);
        stock.addStock(stockItem, 50);
        assertTrue(stock.getStockItem(11).isPresent());
    }

    @Test
    public void testIsItemInStock() throws Exception {
        assertFalse(stock.isItemInStock(99));
    }

    @Test
    public void testLessStockItem() throws SupermarketServiceException {
        StockItem stockItem = createStockItem(11L, "Soup", UnitPrice.price(1.99d), false);
        stock.addStock(stockItem, 50);
        assertEquals(stock.lessStockItem(stockItem), 49);
    }


}