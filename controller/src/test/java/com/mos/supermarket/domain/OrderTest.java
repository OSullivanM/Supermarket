package com.mos.supermarket.domain;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

import com.mos.supermarket.TestHelper;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.util.List;

public class OrderTest {

    private Order order = Order.getInstance();

    @AfterMethod
    public void tearDown() throws Exception {
        order.clearAllOrders();
    }

    @Test
    public void testCreateOrder() throws Exception {
        final long orderId = 11L;
        final OrderItem orderItem = OrderItem.createOrderItem(TestHelper.createStockItem(55L, "My"
            + " Item", UnitPrice.price(5d), false), 100);
        Order myOrder = order.getOrCreateOrder(orderId, orderItem);
        assertNotNull(myOrder);
    }

    @Test
    public void testGetMyOrder() throws Exception {
        final long orderId = 11L;
        final OrderItem orderItem = OrderItem.createOrderItem(TestHelper.createStockItem(55L, "My"
            + " Item", UnitPrice.price(5d), false), 100);
        Order myOrder = order.getOrCreateOrder(orderId, orderItem);
        assertNotNull(myOrder);
        List<OrderItem> actual = myOrder.getOrderItems(orderId);
        assertEquals(actual.get(0).getStockItem().getItemName(), "My Item");
        assertEquals(actual.size(), 1);
    }


}