package com.mos.supermarket.offers;

import com.mos.supermarket.domain.Item;
import com.mos.supermarket.enums.OfferType;
import com.mos.supermarket.enums.SupermarketServiceExceptionEnum;
import com.mos.supermarket.exception.SupermarketServiceException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class OfferTest {

    private Offer offer;
    private static final String OFFER_NAME = "foo";
    private static final String OFFER_DESCRIPTION = "bar";

    @BeforeMethod
    public void setUp() throws Exception {
    }

    @AfterMethod
    public void tearDown() throws Exception {

    }

    @Test
    public void testCreateOffer_OK() throws SupermarketServiceException {
        final OfferType offerType = OfferType.BUY_X_GET_Y_FREE;
        final Item item = new Item(66L, "My Item");
        offer = Offer.createOffer(OFFER_NAME, OFFER_DESCRIPTION, item, offerType, 3, 1,
            null, null, null);
        assertNotNull(offerType);
    }

    @Test
    public void testCreateOffer_MandatoryNotSet() throws SupermarketServiceException {
        final OfferType offerType = OfferType.BUY_X_GET_Y_FREE;
        try {
            assertNull(Offer.createOffer(OFFER_NAME, OFFER_DESCRIPTION, null, offerType, 3, 1,
                null, null, null));
        } catch (SupermarketServiceException e) {
            assertEquals(e.getErrorCode(), SupermarketServiceExceptionEnum.VALIDATION_ERROR);
        }
    }

    @Test
    public void testCreateOffer_CombinationNotMet() throws SupermarketServiceException {
        final OfferType offerType = OfferType.BUY_X_GET_Y_FREE;
        final Item item = new Item(66L, "My Item");
        try {
            assertNull(Offer.createOffer(OFFER_NAME, OFFER_DESCRIPTION, item, offerType, null, null,
                null, null, null));
        } catch (SupermarketServiceException e) {
            assertEquals(e.getErrorCode(), SupermarketServiceExceptionEnum.VALIDATION_ERROR);
            assertEquals(e.getErrorDetails(), "Missing fields for an offer of this type.");
        }
    }
}