package com.mos.supermarket.exception;

import com.mos.supermarket.enums.SupermarketServiceExceptionEnum;

/**
 * Created by mosul on 07/05/2017.
 */
public class SupermarketServiceException extends Throwable {

    private final String errorDetails;
    private final SupermarketServiceExceptionEnum errorCode;

    public SupermarketServiceException(SupermarketServiceExceptionEnum errorCode, String errorDetails) {
        this.errorDetails = errorDetails;
        this.errorCode = errorCode;
    }

    public String getErrorDetails() {
        return errorDetails;
    }

    public SupermarketServiceExceptionEnum getErrorCode() {
        return errorCode;
    }
}
