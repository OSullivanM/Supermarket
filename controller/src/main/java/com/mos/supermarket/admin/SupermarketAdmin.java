package com.mos.supermarket.admin;

import com.mos.supermarket.domain.Item;
import com.mos.supermarket.domain.StockItem;
import com.mos.supermarket.exception.SupermarketServiceException;
import com.mos.supermarket.offers.Offer;
import com.mos.supermarket.enums.OfferType;

import java.time.LocalDateTime;

/**
 * Created by mosul on 07/05/2017.
 */
public interface SupermarketAdmin {

    Offer createOffer(String offerName, String offerDescription, Item offerToItem, OfferType
        offerType, Integer unitsToBuy, Integer unitsFree, Double setPrice, Double percentageDiscount, LocalDateTime offerExpire) throws SupermarketServiceException;

    void addStock(StockItem stockItem, long units) throws SupermarketServiceException;

    void deleteOfferOnItem(long itemId) throws SupermarketServiceException;
}
