package com.mos.supermarket.admin;

import com.mos.supermarket.domain.Item;
import com.mos.supermarket.domain.Stock;
import com.mos.supermarket.domain.StockItem;
import com.mos.supermarket.exception.SupermarketServiceException;
import com.mos.supermarket.offers.Offer;
import com.mos.supermarket.enums.OfferType;
import com.mos.supermarket.offers.OfferStore;

import java.time.LocalDateTime;

/**
 * Created by mosul on 07/05/2017.
 */
public class SupermarketAdminImpl implements SupermarketAdmin {

    private Stock stock = Stock.getInstance();
    private OfferStore offerStore = OfferStore.getInstance();

    @Override
    public Offer createOffer(String offerName, String offerDescription, Item offerToItem, OfferType
        offerType, Integer unitsToBuy, Integer unitsFree, Double discountedPriceTotal, Double percentageDiscount, LocalDateTime offerExpire) throws SupermarketServiceException {
        return Offer.createOffer(offerName, offerDescription, offerToItem, offerType, unitsToBuy,
            unitsFree, discountedPriceTotal, percentageDiscount, offerExpire);
    }

    @Override
    public void addStock(StockItem stockItem, long units) throws SupermarketServiceException {
        stock.addStock(stockItem, units);
    }

    @Override
    public void deleteOfferOnItem(long itemId) throws SupermarketServiceException {
        offerStore.removeOffer(itemId);
    }
}
