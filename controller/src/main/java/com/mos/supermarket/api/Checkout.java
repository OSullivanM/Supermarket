package com.mos.supermarket.api;

import com.mos.supermarket.domain.Order;
import com.mos.supermarket.domain.OrderCheckout;
import com.mos.supermarket.enums.OfferType;
import com.mos.supermarket.exception.SupermarketServiceException;
import com.mos.supermarket.offers.MultiBuyOfferCallback;
import com.mos.supermarket.offers.Offer;
import com.mos.supermarket.offers.OfferStore;
import com.mos.supermarket.offers.SetPriceDiscountOfferCallback;

import java.util.Optional;

/**
 * Created by mosul on 12/05/2017.
 */
public final class Checkout {

    private static Checkout checkout = null;

    public static Checkout getInstance() {
        if (checkout == null) {
            return new Checkout();
        }
        return checkout;
    }

    public OrderCheckout checkoutItems(long orderId, Order order) throws SupermarketServiceException {

        final double subTotal = order.getSubTotal(orderId);
        final double[] savings = {0D};
        order.getOrderItems(orderId).forEach(item -> {
            Optional<Offer> offer = OfferStore.getInstance().getOfferByItem(item.getStockItem().getItemId());
            if (offer.isPresent()) {
                OfferType offerType = offer.get().getOfferType();
                switch (offerType) {
                    case BUY_X_GET_Y_FREE:
                        MultiBuyOfferCallback multiBuyOfferCallback = new MultiBuyOfferCallback();
                        savings[0] += multiBuyOfferCallback.calculateOfferSavings(offer.get(), item);
                        break;
                    case BUY_X_FOR_SET_PRICE:
                        SetPriceDiscountOfferCallback setPriceDiscountOfferCallback = new SetPriceDiscountOfferCallback();
                        savings[0] += setPriceDiscountOfferCallback.calculateOfferSavings(offer.get(), item);
                        break;
                    case X_PERCENT_OFF:
                        //TODO: implement
                        break;
                }
            }
        });
        final double total = subTotal - savings[0];
        return new OrderCheckout(orderId, subTotal, total, savings[0]);
    }
}
