package com.mos.supermarket.api;

import com.mos.supermarket.domain.*;
import com.mos.supermarket.exception.SupermarketServiceException;

import java.util.Optional;

import static com.mos.supermarket.domain.Stock.getInstance;
import static com.mos.supermarket.enums.SupermarketServiceExceptionEnum.ITEM_NOT_IN_STOCK;

/**
 * Created by mosul on 07/05/2017.
 */
public class SupermarketServiceImpl implements SupermarketService {

    private final Order order = Order.getInstance();
    private final Stock stock = Stock.getInstance();
    private final Checkout checkout = Checkout.getInstance();

    @Override
    public OrderCheckout checkout(long orderId) throws SupermarketServiceException {
        Order thisOrder = order.getOrCreateOrder(orderId, null);
        return checkout.checkoutItems(orderId, thisOrder);
    }

    @Override
    public Order scanAndAddToOrder(long orderId, long itemId, double quantity) throws SupermarketServiceException {

        Order thisOrder;
        Optional<StockItem> stockItem = getInstance().getStockItem(itemId);
        if (stockItem.isPresent()) {
            thisOrder = order.getOrCreateOrder(orderId, OrderItem.createOrderItem(stockItem.get(), quantity));
            stock.lessStockItem(stockItem.get());
        } else {
            throw new SupermarketServiceException(ITEM_NOT_IN_STOCK, ITEM_NOT_IN_STOCK.getErrorDetails());
        }
        return thisOrder;
    }

    @Override
    public void removeFromOrder(long orderId, Long itemId) throws SupermarketServiceException {
        order.removeItem(orderId, itemId);
    }

    @Override
    public void clearOrder(long orderId) throws SupermarketServiceException {
        order.clearOrder(orderId);
    }

    @Override
    public boolean itemInStock(long itemId) throws SupermarketServiceException {
        return stock.isItemInStock(itemId);
    }

    @Override
    public StockItem scan(long itemId) throws SupermarketServiceException {
        return null;
    }
}
