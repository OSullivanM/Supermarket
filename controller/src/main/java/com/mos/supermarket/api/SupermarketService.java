package com.mos.supermarket.api;

import com.mos.supermarket.domain.Order;
import com.mos.supermarket.domain.OrderCheckout;
import com.mos.supermarket.domain.StockItem;
import com.mos.supermarket.exception.SupermarketServiceException;

/**
 * Created by mosul on 06/05/2017.
 */
public interface SupermarketService {

    OrderCheckout checkout(long orderId) throws SupermarketServiceException;

    StockItem scan(long barCode) throws SupermarketServiceException;

    Order scanAndAddToOrder(long orderId, long itemId, double quantity) throws
        SupermarketServiceException;

    void removeFromOrder(long orderId, Long barCode) throws SupermarketServiceException;

    void clearOrder(long orderId) throws SupermarketServiceException;

    boolean itemInStock(long itemId) throws SupermarketServiceException;
}
