package com.mos.supermarket.offers;

import com.mos.supermarket.domain.Item;
import com.mos.supermarket.enums.OfferType;
import com.mos.supermarket.enums.SupermarketServiceExceptionEnum;
import com.mos.supermarket.exception.SupermarketServiceException;

import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicLong;

public final class Offer extends AbstractOffer {

    private final Item offerToItem;
    private final OfferType offerType;
    private Integer unitsToBuy;
    private Integer unitsFree;
    private Double discountedPriceTotal;
    private Double percentageDiscount;
    private LocalDateTime offerExpire;
    private static AtomicLong offerId = new AtomicLong();
    private static OfferStore offerStore = OfferStore.getInstance();

    private Offer(Long offerId, String offerName, String offerDescription, Item offerToItem,
                  OfferType offerType) {
        super(offerId, offerName, offerDescription);
        this.offerToItem = offerToItem;
        this.offerType = offerType;
    }

    public static Offer createOffer(String offerName, String offerDescription, Item
        offerToItem, OfferType offerType, Integer unitsToBuy, Integer unitsFree,
                                    Double discountedPriceTotal, Double percentageDiscount, LocalDateTime offerExpire)
        throws SupermarketServiceException {

        if (offerName == null || offerDescription == null || offerToItem ==null) {
            throw new SupermarketServiceException(SupermarketServiceExceptionEnum
                .VALIDATION_ERROR, "Mandatory fields must not be empty");
        }
        validateOfferFields(offerType, unitsToBuy, unitsFree, discountedPriceTotal, percentageDiscount);
        Offer offer = new Offer(offerId.incrementAndGet(), offerName, offerDescription, offerToItem, offerType)
                .unitsToBuy(unitsToBuy)
                .unitsFree(unitsFree)
                .discountedPriceTotal(discountedPriceTotal)
                .percentageDiscount(percentageDiscount)
                .offerExpire(offerExpire)
                .build();
        offerStore.addOffer(offer);
        return offer;
    }

    private Offer unitsToBuy(Integer unitsToBuy) {
        this.unitsToBuy = unitsToBuy;
        return this;
    }

    private Offer unitsFree(Integer unitsFree) {
        this.unitsFree = unitsFree;
        return this;
    }

    private Offer discountedPriceTotal(Double discountedPriceTotal) {
        this.discountedPriceTotal = discountedPriceTotal;
        return this;
    }

    private Offer percentageDiscount(Double percentageDiscount) {
        this.percentageDiscount = percentageDiscount;
        return this;
    }

    private Offer offerExpire(LocalDateTime offerExpire) {
        this.offerExpire = offerExpire;
        return this;
    }

    private Offer build() {
        setDiscountedPriceTotal(discountedPriceTotal);
        setOfferExpire(offerExpire);
        setPercentageDiscount(percentageDiscount);
        setUnitsFree(unitsFree);
        setUnitsToBuy(unitsToBuy);
        return this;
    }

    private static boolean validateOfferFields(OfferType offerType, Integer unitsToBuy, Integer
        unitsFree, Double discountedPriceTotal, Double percentageDiscount) throws SupermarketServiceException {
        switch (offerType) {
            case BUY_X_GET_Y_FREE:
                if (unitsToBuy == null && unitsFree == null) {
                    throw new SupermarketServiceException(SupermarketServiceExceptionEnum
                        .VALIDATION_ERROR, "Missing fields for an offer of this type.");
                }
                return false;
            case BUY_X_FOR_SET_PRICE:
                if (unitsToBuy == null && discountedPriceTotal == null) {
                    throw new SupermarketServiceException(SupermarketServiceExceptionEnum
                        .VALIDATION_ERROR, "Missing fields for an offer of this type.");
                }
                return false;
            case X_PERCENT_OFF:
                if (percentageDiscount == null) {
                    throw new SupermarketServiceException(SupermarketServiceExceptionEnum
                        .VALIDATION_ERROR, "Missing fields for an offer of this type.");
                }
                return false;
        }
        return true;
    }


    public Item getOfferToItem() {
        return offerToItem;
    }

    public OfferType getOfferType() {
        return offerType;
    }

    public Integer getUnitsToBuy() {
        return unitsToBuy;
    }

    public void setUnitsToBuy(Integer unitsToBuy) {
        this.unitsToBuy = unitsToBuy;
    }

    public Integer getUnitsFree() {
        return unitsFree;
    }

    public void setUnitsFree(Integer unitsFree) {
        this.unitsFree = unitsFree;
    }

    public Double getDiscountedPriceTotal() {
        return discountedPriceTotal;
    }

    public void setDiscountedPriceTotal(Double discountedPriceTotal) {
        this.discountedPriceTotal = discountedPriceTotal;
    }

    public Double getPercentageDiscount() {
        return percentageDiscount;
    }

    public void setPercentageDiscount(Double percentageDiscount) {
        this.percentageDiscount = percentageDiscount;
    }

    public LocalDateTime getOfferExpire() {
        return offerExpire;
    }

    public void setOfferExpire(LocalDateTime offerExpire) {
        this.offerExpire = offerExpire;
    }
}
