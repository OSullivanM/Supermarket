package com.mos.supermarket.offers;

import com.mos.supermarket.enums.OfferType;

/**
 * Created by mosul on 07/05/2017.
 */
public abstract class AbstractOffer {

    private final long offerId;
    private final String offerName;
    private final String offerDescription;

    public AbstractOffer(Long offerId, String offerName, String offerDescription) {
        this.offerId = offerId;
        this.offerName = offerName;
        this.offerDescription = offerDescription;
    }

    public long getOfferId() {
        return offerId;
    }

    public String getOfferName() {
        return offerName;
    }

    public String getOfferDescription() {
        return offerDescription;
    }

}
