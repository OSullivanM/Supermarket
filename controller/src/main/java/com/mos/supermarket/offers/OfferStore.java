package com.mos.supermarket.offers;

import com.mos.supermarket.exception.SupermarketServiceException;

import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public final class OfferStore {

    private static OfferStore instance = null;

    public static OfferStore getInstance() {
        if (instance == null) {
            instance = new OfferStore();
        }
        return instance;
    }

    private final Set<Offer> offers;

    private OfferStore() {
        this.offers = ConcurrentHashMap.newKeySet();
    }

    public void addOffer(Offer offer) {
        offers.add(offer);
    }
    public Set<Offer> getOffers() {
        return offers;
    }

    public Optional<Offer> getOffer(long offerId) {
        return offers.stream().filter(offer -> offer.getOfferId() == offerId)
            .findAny();
    }

    public Optional<Offer> getOfferByItem(long itemId) {
        return offers.stream().filter(offer -> offer.getOfferToItem().getItemId() == itemId).findFirst();
    }

    public void removeOffer(long itemID) throws SupermarketServiceException {
        Optional<Offer> offer = offers.stream()
                .filter(o -> o.getOfferToItem().getItemId() == itemID).findAny();
        offer.ifPresent(offer1 -> offers.remove(offer.get()));
    }
}
