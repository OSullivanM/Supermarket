package com.mos.supermarket.offers;

import com.mos.supermarket.domain.OrderItem;
import com.mos.supermarket.utils.OfferAvailableCompute;

/**
 * Created by mosul on 13/05/2017.
 */
public class MultiBuyOfferCallback implements OfferAvailableCompute {

    @Override
    public double calculateOfferSavings(Offer offer, OrderItem orderItem) {
        double savings = 0D;
        if (orderItem.getQuantity() >= offer.getUnitsToBuy()) {
            return offer.getUnitsFree() * orderItem.getStockItem().getUnitPrice().getPrice();
        }
        return savings;
    }
}
