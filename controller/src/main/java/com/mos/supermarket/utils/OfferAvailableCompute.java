package com.mos.supermarket.utils;

import com.mos.supermarket.domain.OrderItem;
import com.mos.supermarket.offers.Offer;

/**
 * Created by mosul on 09/05/2017.
 */
@FunctionalInterface
public interface OfferAvailableCompute {
    double calculateOfferSavings(Offer offer, OrderItem orderItem);
}
