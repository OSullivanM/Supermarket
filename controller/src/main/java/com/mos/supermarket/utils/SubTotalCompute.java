package com.mos.supermarket.utils;

import com.mos.supermarket.exception.SupermarketServiceException;

/**
 * Created by mosul on 08/05/2017.
 */
@FunctionalInterface
public interface SubTotalCompute {
    double getSubTotal(long orderId) throws SupermarketServiceException;
}
