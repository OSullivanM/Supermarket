package com.mos.supermarket.enums;

/**
 * Created by mosul on 07/05/2017.
 */
public enum SupermarketServiceExceptionEnum {

    ITEM_NOT_IN_STOCK("Item is not longer in stock."),
    ITEM_NOT_FOUND("Warning: Item not found in system"),
    VALIDATION_ERROR("Validation Error"),
    OFFER_NOT_FOUND("Offer not found for given id"),
    ORDER_NOT_FOUND("Please contact management");

    private final String errorDetails;

    SupermarketServiceExceptionEnum(String errorDetails) {
        this.errorDetails = errorDetails;
    }

    public String getErrorDetails() {
        return errorDetails;
    }

    @Override
    public String toString() {
        return "SupermarketServiceExceptionEnum{" +
                "errorDetails='" + errorDetails + '\'' +
                '}';
    }
}
