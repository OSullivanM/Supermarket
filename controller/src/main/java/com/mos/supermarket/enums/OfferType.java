package com.mos.supermarket.enums;

/**
 * Created by mosul on 09/05/2017.
 */
public enum OfferType {
    BUY_X_GET_Y_FREE, BUY_X_FOR_SET_PRICE, X_PERCENT_OFF
}
