package com.mos.supermarket.domain;

/**
 * Created by mosul on 08/05/2017.
 */
public class OrderItem {

    private final StockItem stockItem;
    private final double quantity;

    private OrderItem(StockItem stockItem, double quantity) {
        this.stockItem = stockItem;
        this.quantity = quantity;
    }

    public static OrderItem createOrderItem(StockItem item, double quantity) {
        return new OrderItem(item, quantity);
    }

    public double getQuantity() {
        return quantity;
    }

    public StockItem getStockItem() {
        return stockItem;
    }

    @Override
    public String toString() {
        return "OrderItem{" +
                "stockItem=" + stockItem +
                ", quantity=" + quantity +
                '}';
    }
}
