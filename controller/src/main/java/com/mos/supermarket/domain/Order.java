package com.mos.supermarket.domain;

import com.google.common.collect.Lists;
import com.mos.supermarket.exception.SupermarketServiceException;
import com.mos.supermarket.utils.SubTotalCompute;
import org.testng.collections.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.mos.supermarket.enums.SupermarketServiceExceptionEnum.ITEM_NOT_FOUND;
import static com.mos.supermarket.enums.SupermarketServiceExceptionEnum.ORDER_NOT_FOUND;

/**
 * Created by mosul on 07/05/2017.
 */
public class Order implements SubTotalCompute {

    private static Order instance = null;

    public static Order getInstance() {
        if (instance == null) {
            instance = new Order();
        }
        return instance;
    }

    private final Map<Long, List<OrderItem>> orders = new HashMap<>();

    public Map<Long, List<OrderItem>> getOrders() {
        return orders;
    }

    public List<OrderItem> getOrderItems(long orderId) {
        return orders.get(orderId);
    }

    public Order getOrCreateOrder(Long orderId, OrderItem item) {
        List<OrderItem> orderItems = getOrders().get(orderId);
        if (CollectionUtils.hasElements(orderItems) && item != null) {
            orderItems.add(item);
            orders.put(orderId, orderItems);
        } else if (!CollectionUtils.hasElements(orderItems) && item != null) {
            orders.put(orderId, Lists.newArrayList(item));
        }
        return this;
    }

    public void removeItem(long orderId, long itemId) throws SupermarketServiceException {
        OrderItem orderItem = Optional.ofNullable(getOrders().get(orderId))
                .orElseThrow(() -> new SupermarketServiceException(ORDER_NOT_FOUND, ORDER_NOT_FOUND.getErrorDetails()))
                .stream().filter(i -> i.getStockItem().getItemId() == itemId).findAny()
                .orElseThrow( () -> new SupermarketServiceException(ITEM_NOT_FOUND, ITEM_NOT_FOUND.getErrorDetails()));
        orders.get(orderId).remove(orderItem);
    }

    public void clearAllOrders() {
        orders.clear();
    }

    public void clearOrder(long orderId) {
        orders.remove(orderId);
    }

    @Override
    public double getSubTotal(long orderId) throws SupermarketServiceException {
        return Optional.ofNullable(getOrderItems(orderId)).orElseThrow(() -> new SupermarketServiceException(ORDER_NOT_FOUND, ORDER_NOT_FOUND.getErrorDetails())).stream()
                .mapToDouble(price -> price.getStockItem().getUnitPrice().getPrice() * price.getQuantity())
                .sum();
    }


}
