package com.mos.supermarket.domain;

/**
 * Created by mosul on 12/05/2017.
 */
public final class OrderCheckout{

    private final long orderId;
    private final Double subTotal;
    private final Double total;
    private final Double savings;

    public OrderCheckout(long orderId, Double subTotal, Double total, Double savings) {
        this.orderId = orderId;
        this.subTotal = subTotal;
        this.total = total;
        this.savings = savings;
    }

    public long getOrderId() {
        return orderId;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public Double getTotal() {
        return total;
    }

    public Double getSavings() {
        return savings;
    }
}
