package com.mos.supermarket.domain;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Created by mosul on 07/05/2017.
 */
public final class UnitPrice {

    private final BigDecimal price;

    private UnitPrice(BigDecimal price) {
        this.price = price;
    }

    public static UnitPrice price(double price) {
        return new UnitPrice(BigDecimal.valueOf(price));
    }

    public Double getPrice() {
        return price.setScale(2, BigDecimal.ROUND_UP).doubleValue();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UnitPrice unitPrice = (UnitPrice) o;
        return Objects.equals(price, unitPrice.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(price);
    }
}
