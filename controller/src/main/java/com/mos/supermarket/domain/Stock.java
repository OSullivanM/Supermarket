package com.mos.supermarket.domain;

import com.mos.supermarket.exception.SupermarketServiceException;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import static com.mos.supermarket.enums.SupermarketServiceExceptionEnum.ITEM_NOT_FOUND;

/**
 * Created by mosul on 07/05/2017.
 */
public class Stock {

    private static Stock instance = null;

    public static Stock getInstance() {
        if (instance == null) {
            instance = new Stock();
        }
        return instance;
    }

    private final Map<StockItem, AtomicLong> stock = new ConcurrentHashMap<>();

    public void addStock(StockItem stockItem, long numUnits) {
        stock.putIfAbsent(stockItem, new AtomicLong(0));
        stock.get(stockItem).addAndGet(numUnits);
    }

    public Optional<StockItem> getStockItem(long itemId) {
        return stock.entrySet().stream()
                .filter(item -> item.getKey().getItemId().equals(itemId))
                .map(Map.Entry::getKey)
                .findFirst();
    }

    public boolean isItemInStock(long itemId) {
        return stock.entrySet().stream()
                .anyMatch(e -> e.getKey().getItemId().equals(itemId));
    }

    public long lessStockItem(StockItem stockItem) throws SupermarketServiceException {
        AtomicLong quantityRemaining = stock.get(stockItem);
        if (quantityRemaining != null) {
            return quantityRemaining.decrementAndGet();
        } else {
            throw new SupermarketServiceException(ITEM_NOT_FOUND, ITEM_NOT_FOUND.getErrorDetails());
        }
    }

    public void clearAllStock() {
        stock.clear();
    }

    public int getTotalStockSize() {
        return stock.size();
    }
}
