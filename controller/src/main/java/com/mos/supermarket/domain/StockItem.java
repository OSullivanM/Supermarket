package com.mos.supermarket.domain;

import java.util.Objects;

/**
 * Created by mosul on 07/05/2017.
 */
public class StockItem extends Item {

    private final UnitPrice unitPrice;
    private final boolean offerAvailable;

    public StockItem(Long itemId, String itemName, UnitPrice price, boolean offerAvailable) {
        super(itemId, itemName);
        this.unitPrice = price;
        this.offerAvailable = offerAvailable;
    }

    public UnitPrice getUnitPrice() {
        return unitPrice;
    }

    public boolean isOfferAvailable() {
        return offerAvailable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        StockItem stockItem = (StockItem) o;
        return offerAvailable == stockItem.offerAvailable &&
                Objects.equals(unitPrice, stockItem.unitPrice);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), unitPrice, offerAvailable);
    }

    @Override
    public String toString() {
        return "StockItem{" +
                "unitPrice=" + unitPrice +
                ", offerAvailable=" + offerAvailable +
                '}';
    }
}
