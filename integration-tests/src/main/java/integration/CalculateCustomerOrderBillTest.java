package integration;

import com.mos.supermarket.domain.*;
import com.mos.supermarket.enums.OfferType;
import com.mos.supermarket.exception.SupermarketServiceException;
import integration.utils.TestHelper;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

/**
 * Created by mosul on 07/05/2017.
 */
public class CalculateCustomerOrderBillTest extends AbstractIntegrationTestHelper {

    private static final long ORDER_ID = 1L;
    private static final long ITEM1_ID = 11L;
    private static final long ITEM2_ID = 22L;
    private static final long ITEM3_ID = 33L;

    @BeforeMethod
    public void testSetup() throws SupermarketServiceException {
        supermarketAdmin.addStock(createStockItem(ITEM1_ID, "Apple", UnitPrice.price(.1d), false), 1);
        supermarketAdmin.addStock(createStockItem(ITEM2_ID, "Pear", UnitPrice.price(.3d), false), 1);
        supermarketAdmin.addStock(createStockItem(ITEM3_ID, "Beans", UnitPrice.price(1.1d), false), 500);
    }

    @AfterMethod
    public void tearDown() throws SupermarketServiceException {
        supermarketService.clearOrder(ORDER_ID);
        supermarketAdmin.deleteOfferOnItem(ITEM3_ID);
    }
    @Test
    public void testCheckoutSingleItem() throws SupermarketServiceException {
        Order myOrder = supermarketService.scanAndAddToOrder(ORDER_ID, ITEM1_ID, 1d);
        Order expectedOrder = Order.getInstance().getOrCreateOrder(ORDER_ID, OrderItem.createOrderItem(createStockItem(ITEM1_ID, "Apple", UnitPrice.price(.1d), false), 1));

        assertEquals(myOrder, expectedOrder, "Orders do not match");
        assertEquals(myOrder.getSubTotal(ORDER_ID), expectedOrder.getSubTotal(ORDER_ID));
    }

    @Test
    public void testCheckoutMultipleItems() throws SupermarketServiceException {
        supermarketService.scanAndAddToOrder(ORDER_ID, ITEM1_ID, 1);
        Order myOrder = supermarketService.scanAndAddToOrder(ORDER_ID, ITEM2_ID, 1);

        assertEquals(myOrder.getOrderItems(ORDER_ID).size(), 2);
        assertEquals(myOrder.getSubTotal(ORDER_ID), .4D, DELTA);
    }

    @Test
    public void testCheckoutOrderWithOfferA() throws SupermarketServiceException {
        Item beans = new Item(ITEM3_ID, "Beans");
        supermarketAdmin.createOffer("MultiSave Offer", "Buy 2 Get 1 Free", beans, OfferType.BUY_X_GET_Y_FREE, 2, 1, null, null, null);
        supermarketService.scanAndAddToOrder(ORDER_ID, beans.getItemId(), 3);
        OrderCheckout orderCheckout = supermarketService.checkout(ORDER_ID);
        assertEquals(orderCheckout.getOrderId(), ORDER_ID);
        assertEquals(orderCheckout.getSubTotal(), 3.3D, DELTA);
        assertEquals(orderCheckout.getTotal(), 2.2D, DELTA);
        assertEquals(orderCheckout.getSavings(), 1.1D);
    }

    @Test
    public void testCheckoutOrderWithOfferB() throws SupermarketServiceException {
        Item beans = new Item(ITEM3_ID, "Beans");
        supermarketAdmin.createOffer("MultiSave Offer", "3 For 1 Pound", beans, OfferType.BUY_X_FOR_SET_PRICE, 3, null, 2D, null, null);
        supermarketService.scanAndAddToOrder(ORDER_ID, beans.getItemId(), 3);
        OrderCheckout orderCheckout = supermarketService.checkout(ORDER_ID);
        assertEquals(orderCheckout.getOrderId(), ORDER_ID);
        assertEquals(orderCheckout.getSubTotal(), 3.3D, DELTA);
        assertEquals(orderCheckout.getTotal(), 2D, DELTA);
        assertEquals(orderCheckout.getSavings(), 1.3, DELTA);
    }

    @Test
    public void testCheckoutOrderWithMultipl() throws SupermarketServiceException {
        Item beans = new Item(ITEM3_ID, "Beans");
        supermarketAdmin.createOffer("MultiSave Offer", "3 For 1 Pound", beans, OfferType.BUY_X_FOR_SET_PRICE, 3, null, 2D, null, null);
        supermarketService.scanAndAddToOrder(ORDER_ID, beans.getItemId(), 3);
        OrderCheckout orderCheckout = supermarketService.checkout(ORDER_ID);
        assertEquals(orderCheckout.getOrderId(), ORDER_ID);
        assertEquals(orderCheckout.getSubTotal(), 3.3D, DELTA);
        assertEquals(orderCheckout.getTotal(), 2D, DELTA);
        assertEquals(orderCheckout.getSavings(), 1.3, DELTA);
    }

    private StockItem createStockItem(long itemId, String itemName, UnitPrice unitPrice, boolean offerAvailable) {
        return TestHelper.createStockItem(itemId, itemName, unitPrice, offerAvailable);
    }
}
