package integration.utils;

import com.mos.supermarket.domain.StockItem;
import com.mos.supermarket.domain.UnitPrice;

/**
 * Created by mosul on 07/05/2017.
 */
public class TestHelper {

    public static StockItem createStockItem(long itemId, String itemName, UnitPrice price, boolean offerAvailable) {
        return new StockItem(itemId, itemName, price, offerAvailable);
    }

}
