package integration;

import com.mos.supermarket.admin.SupermarketAdminImpl;
import com.mos.supermarket.api.SupermarketServiceImpl;
import org.testng.annotations.BeforeSuite;

/**
 * Created by mosul on 07/05/2017.
 */
public abstract class AbstractIntegrationTestHelper {

    protected static final double DELTA = 0.0000001;
    protected SupermarketServiceImpl supermarketService;
    protected SupermarketAdminImpl supermarketAdmin;

    @BeforeSuite
    public void before() throws Exception {
        supermarketService = new SupermarketServiceImpl();
        supermarketAdmin = new SupermarketAdminImpl();
    }

}
